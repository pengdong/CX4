﻿using Chloe;

namespace CX4.Chloe
{
    /// <summary>
    /// 写库上下文接口
    /// </summary>
    public interface IDbWriterContext : IDbContext
    {
    }
}

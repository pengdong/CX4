﻿using Chloe;

namespace CX4.Chloe
{
    /// <summary>
    /// 读库上下文接口
    /// </summary>
    public interface IDbReaderContext : IDbContext
    {
    }
}

using System;
using System.Collections.Generic;
using System.IO;

using IGeekFan.AspNetCore.Knife4jUI;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;

using QXQ.FMS.API.Conventions;
using QXQ.FMS.API.Filters;
using QXQ.FMS.Application.Options;

using StackExchange.Redis;

namespace QXQ.FMS.API
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// 
        /// </summary>
        public string RouterPrefix => Configuration.GetValue<string>("Router:ApiPrefix");

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<PayOptions>(Configuration.GetSection("Pay"));
            services.Configure<UploadFileOptions>(Configuration.GetSection("File"));
            services.Configure<DomainOptions>(Configuration.GetSection("Domain"));
            services.AddControllers(option =>
            {
                option.Filters.Add(typeof(ExceptionHandlerAttribute));
                option.Filters.Add(typeof(ResultHandlerAttribute));
                option.Conventions.Add(new RouteConvention(RouterPrefix));
            });
            services.AddHttpClient();
            services.AddAutoMapper();
            services.AddFluentValidation();
            services.AddMediatR();
            ConfigureRedis(services);
            ConfigureDBContext(services);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "QXQ.FMS.API", Version = "v1" });
                c.CustomOperationIds(apiDesc =>
                {
                    var controllerAction = apiDesc.ActionDescriptor as ControllerActionDescriptor;
                    return controllerAction.ControllerName + "-" + controllerAction.ActionName;
                });
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                string xmlApiPath = Path.Combine(basePath, "QXQ.FMS.API.xml");
                string xmlAppPath = Path.Combine(basePath, "QXQ.FMS.Application.xml");
                if (File.Exists(xmlApiPath))
                {
                    c.IncludeXmlComments(xmlApiPath, true);
                }
                if (File.Exists(xmlApiPath))
                {
                    c.IncludeXmlComments(xmlAppPath, true);
                }
            });
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(k =>
                {
                    k.AllowAnyOrigin();
                    k.AllowAnyHeader();
                    k.AllowAnyMethod();
                });
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        private void ConfigureDBContext(IServiceCollection services)
        {
            var dbReaderContext = Configuration.GetValue<string>("Oracle:Reader");
            var dbWriterContext = Configuration.GetValue<string>("Oracle:Writer");
            services.AddChloeDbContext(k => k.UseOracle(dbReaderContext, dbWriterContext));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        private void ConfigureRedis(IServiceCollection services)
        {
            var redisOptions = services.ConfigureOptions<RedisOptions>(Configuration.GetSection("Redis"));
            services.AddRedisCache(k =>
            {
                var options = ConfigurationOptions.Parse(redisOptions.ConnectionString);
                options.DefaultDatabase = redisOptions.Database;
                k.ConfigurationOptions = options;
                k.InstanceName = redisOptions.InstanceName;
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseKnife4UI(c =>
            {
                c.RoutePrefix = string.IsNullOrWhiteSpace(RouterPrefix) ? "swagger" : RouterPrefix;
                c.SwaggerEndpoint("/v1/api-docs", "QXQ.FMS.API v1");
            });
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseCors();
            app.UseAuthorization();
            app.Map(string.IsNullOrWhiteSpace(RouterPrefix) ? "/health" : $"/{RouterPrefix}/health", app =>
                {
                    app.Map("/base", Health);
                    app.Map("/data", Health);
                });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapSwagger("{documentName}/api-docs");
            });
        }

        private void Health(IApplicationBuilder app)
        {
            app.Run(async context => { await context.Response.WriteAsync("ok"); });
        }
    }
}

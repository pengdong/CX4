using System;
using System.Collections.Generic;

using Exceptionless;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Nacos.Microsoft.Extensions.Configuration;

using QXQ.FMS.Application.BackgroundServices;

using Serilog;
using Serilog.Events;

namespace QXQ.FMS.API
{
    /// <summary>
    /// 
    /// </summary>
    public class Program
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(builder =>
                {
                    var config = builder.Build();
                    builder.AddNacosConfiguration(k =>
                    {
                        k.ServerAddresses = new List<string>() { Environment.GetEnvironmentVariable("NACOSBASE_SERVERADDRESS").ToString() };
                        k.DefaultTimeOut = Convert.ToInt32(Environment.GetEnvironmentVariable("NACOSBASE_DEFAULTTIMEOUT").ToString());
                        k.Tenant = Environment.GetEnvironmentVariable("NACOSBASE_NAMESPACE").ToString();
                        k.UserName = Environment.GetEnvironmentVariable("NACOSBASE_USERNAME").ToString();
                        k.Password = Environment.GetEnvironmentVariable("NACOSBASE_PASSWORD").ToString();
                        k.Listeners = config.GetSection("NacosConfig:Listeners").Get<List<ConfigListener>>();
                    });
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureServices(service =>
                {
                    //定时任务
                    service.AddHostedService<PaymentService>();
                    service.AddHostedService<PaymentQueryService>();
                })
                .UseSerilog((hostingContext, services, loggerConfiguration) =>
                {
                    string serverUrl = hostingContext.Configuration.GetValue<string>("ExceptionLess:ServerUrl");
                    string apiKey = hostingContext.Configuration.GetValue<string>("ExceptionLess:FmsApi:ApiKey");
                    int level = hostingContext.Configuration.GetValue<int>("ExceptionLess:FmsApi:LogEventLevel");
                    loggerConfiguration.MinimumLevel.Debug()
                                       .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                                       .Enrich.FromLogContext()
                                       .WriteTo.Console()
                                       .WriteTo.Exceptionless(apiKey, serverUrl: serverUrl, restrictedToMinimumLevel: (LogEventLevel)level);
                });

    }
}

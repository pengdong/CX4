/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *                                                         
 *  创 建 者：pengdong                                                            
 *  创建日期：2020/12/22 8:43:22                                                                
 *  功能描述：PageRequest                                                    
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
*/

namespace CX4.Query
{
    /// <summary>
    /// 
    /// </summary>
    public class PageRequest
    {
        private int _pageIndex = 1;
        private int _pageSize = 20;

        public int PageIndex
        {
            get
            {
                return _pageIndex;
            }
            set
            {
                _pageIndex = value <= 0 ? _pageIndex : value;
            }
        }

        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = value <= 0 ? _pageSize : value;
            }
        }

        /// <summary>
        /// 开始分页数
        /// </summary>
        protected int BeginSize => (PageIndex - 1) * PageSize;

        /// <summary>
        /// 结束分页数
        /// </summary>
        protected int EndSize => PageIndex * PageSize;
    }
}

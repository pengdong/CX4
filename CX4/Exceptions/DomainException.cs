﻿namespace CX4.Exceptions
{
    /// <summary>
    /// 领域层异常
    /// </summary>
    public class DomainException : CX4Exception
    {
        public DomainException(string message) : base(message) { }
    }
}

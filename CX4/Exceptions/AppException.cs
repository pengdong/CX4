﻿namespace CX4.Exceptions
{
    /// <summary>
    /// 应用层异常
    /// </summary>
    public class AppException : CX4Exception
    {
        public AppException(string message) : base(message) { }
    }
}

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *                                                         
 *  创 建 者：pengdong                                                            
 *  创建日期：2020/12/17 11:04:25                                                                
 *  功能描述：ServiceCollectionExtension                                                    
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
*/
using System.Linq;
using System.Reflection;
using CX4.Behaviors;
using FluentValidation;

using MediatR;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// 
    /// </summary>
    public static class ServiceCollectionExtension
    {
        /// <summary>
        /// 
        /// </summary>
        private static Assembly ExecutingAssembly => Assembly.GetExecutingAssembly();

        /// <summary>
        /// 添加MediatR
        /// </summary>
        /// <param name="services"></param>
        public static void AddMediatR(this IServiceCollection services)
        {
            services.AddMediatR(k => k.AsScoped(), ExecutingAssembly);
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidatorBehavior<,>));
            AddQuery(services);
        }

        /// <summary>
        /// 添加FluentValidation
        /// </summary>
        /// <param name="services"></param>
        public static void AddFluentValidation(this IServiceCollection services)
        {
            services.AddValidatorsFromAssembly(ExecutingAssembly, ServiceLifetime.Singleton, k => typeof(IValidator).IsAssignableFrom(k.ValidatorType));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        private static void AddQuery(IServiceCollection services)
        {
            var interfaceList = ExecutingAssembly.ExportedTypes.Where(k => k.IsInterface && k.Name.EndsWith("Query"));
            var implementationList = ExecutingAssembly.GetTypes().Where(k => k.IsClass && k.Name.EndsWith("Query")).ToList();
            foreach (var item in interfaceList)
            {
                implementationList.ForEach(k =>
                {
                    if (k.GetInterfaces().Any(k => k == item))
                    {
                        services.AddScoped(item, k);
                    }
                });
            }
        }
    }
}

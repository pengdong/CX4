﻿/*
 * Author Dong Peng
 *
 * Created at 2021/9/15 12:00:42 
 *
 */
using MediatR;

namespace CX4.Commands
{
    public abstract class BaseCommand<TResponse> : IRequest<TResponse>
    {
    }
}

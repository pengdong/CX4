﻿/*
 * Author Dong Peng
 *
 * Created at 2021/9/13 10:18:41 
 *
 */
using CX4.ResultModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CX4.Extensions
{
    public static class Extension
    {
        /// <summary>
        /// 动态排序法
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="query">IQueryable数据源</param>
        /// <param name="sortColumn">排序的列</param>
        /// <param name="sortType">排序的方法;desc、asc</param>
        /// <returns></returns>
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> query, string sortColumn, string sortType)
        {
            return query.OrderBy(new KeyValuePair<string, string>(sortColumn, sortType));
        }

        /// <summary>
        /// 动态排序法
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="query">数据源</param>
        /// <param name="sort">排序规则，Key为排序列，Value为排序类型</param>
        /// <returns></returns>
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> query, params KeyValuePair<string, string>[] sort)
        {
            var parameter = Expression.Parameter(typeof(T), "k");
            for (var index = 0; index < sort.Length; index++)
            {
                var property = GetTheProperty(typeof(T), sort[index].Key);
                var propertyAccess = Expression.MakeMemberAccess(parameter, property);
                var orderByExp = Expression.Lambda(propertyAccess, parameter);

                var orderName = "";
                if (index > 0)
                {
                    orderName = sort[index].Value.ToLower() == "desc" ? "ThenByDescending" : "ThenBy";
                }
                else
                {
                    orderName = sort[index].Value.ToLower() == "desc" ? "OrderByDescending" : "OrderBy";
                }
                var resultExp = Expression.Call(typeof(Queryable), orderName, new Type[] { typeof(T), property.PropertyType }, query.Expression, Expression.Quote(orderByExp));
                query = query.Provider.CreateQuery<T>(resultExp);
            }
            return (IOrderedQueryable<T>)query;

            //必须追溯到最基类属性
            PropertyInfo GetTheProperty(Type type, string propertyName)
            {
                if (type.BaseType.GetProperties().Any(x => x.Name == propertyName))
                {
                    return GetTheProperty(type.BaseType, propertyName);
                }
                else
                {
                    return type.GetProperty(propertyName);
                }
            }
        }

        /// <summary>
        /// 分页拓展
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="query"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static PageList<TEntity> ToPageList<TEntity>(this IQueryable<TEntity> query, int pageIndex = 1, int pageSize = 20)
            where TEntity : new()
        {
            var totalCount = query.Count();
            var items = query.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            var totalPages = (int)Math.Ceiling(totalCount / (double)pageSize);
            return new PageList<TEntity>
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                Items = items,
                TotalCount = totalCount,
                TotalPages = totalPages,
                HasNextPages = pageIndex < totalPages,
                HasPrevPages = pageIndex - 1 > 0
            };
        }
    }
}

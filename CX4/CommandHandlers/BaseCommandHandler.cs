﻿/*
 * Author Dong Peng
 *
 * Created at 2021/9/14 16:28:59 
 *
 */
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace CX4.CommandHandlers
{
    public abstract class BaseCommandHandler<TCommand, TResponse> : IRequestHandler<TCommand, TResponse> where TCommand : IRequest<TResponse>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<TResponse> Handle(TCommand command, CancellationToken cancellationToken)
        {
            //TODO (log、verify)
            return Task.FromResult(default(TResponse));
        }
    }
}

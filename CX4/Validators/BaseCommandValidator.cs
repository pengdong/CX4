﻿/*
 * Author Dong Peng
 *
 * Created at 2021/9/15 11:23:11 
 *
 */
using FluentValidation;

namespace CX4.Validators
{
    public abstract class BaseCommandValidator<TCommand> : AbstractValidator<TCommand>
    {
        public BaseCommandValidator()
        {
            CascadeMode = CascadeMode.Stop;
        }
    }
}

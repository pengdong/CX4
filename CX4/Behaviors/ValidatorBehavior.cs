﻿using CX4.Exceptions;

using FluentValidation;

using MediatR;

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CX4.Behaviors
{
    /// <summary>
    /// 验证命令行为
    /// </summary>
    /// <typeparam name="TCommand"></typeparam>
    /// <typeparam name="TCommandResult"></typeparam>
    public class ValidatorBehavior<TCommand, TCommandResult> : IPipelineBehavior<TCommand, TCommandResult>
    {
        private readonly IEnumerable<IValidator<TCommand>> _validators;

        public ValidatorBehavior(IEnumerable<IValidator<TCommand>> validators)
        {
            _validators = validators;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public async Task<TCommandResult> Handle(TCommand command, CancellationToken cancellationToken, RequestHandlerDelegate<TCommandResult> next)
        {
            var failures = _validators
              .Select(v => v.Validate(command))
              .SelectMany(result => result.Errors)
              .Where(error => error != null)
              .ToList();
            if (failures.Any())
            {
                throw new AppException(failures.FirstOrDefault().ErrorMessage);
            }
            return await next();
        }
    }
}

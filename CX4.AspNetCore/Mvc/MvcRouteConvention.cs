namespace Microsoft.AspNetCore.Mvc.ApplicationModels
{
    /// <summary>
    /// 
    /// </summary>
    public class RouteConvention : IControllerModelConvention
    {
        private readonly string _routePrefix;

        public RouteConvention(string routePrefix)
        {
            _routePrefix = routePrefix;
        }

        public void Apply(ControllerModel controller)
        {
            if (!string.IsNullOrWhiteSpace(_routePrefix))
            {
                foreach (var item in controller.Selectors)
                {
                    item.AttributeRouteModel = AttributeRouteModel.CombineAttributeRouteModel(new AttributeRouteModel(new RouteAttribute(_routePrefix)), item.AttributeRouteModel);
                }
            }
        }
    }
}

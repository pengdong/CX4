﻿namespace Microsoft.AspNetCore.Mvc
{
    public class ResultObject
    {

        /// <summary>
        /// 
        /// </summary>
        public int Code { get; set; } = 200;

        /// <summary>
        /// 
        /// </summary>
        public dynamic Data { get; set; } = "";

        /// <summary>
        /// 
        /// </summary>
        public string Msg { get; set; } = "";

        /// <summary>
        /// 成功
        /// </summary>
        /// <param name="data"></param>
        public static ResultObject Success(dynamic data)
        {
            return new ResultObject { Data = data };
        }

        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="retStatus"></param>
        /// <returns></returns>
        public static ResultObject Fail(string msg, int code = 500)
        {
            return new ResultObject { Code = code, Msg = msg };
        }
    }
}

﻿/*
 * Author Dong Peng
 *
 * Created at 2021/9/13 10:53:35 
 *
 */
using CX4.Domain.Entity;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Logging;

using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace CX4.EFCore
{
    public class BaseDBContext : DbContext, IUnitOfWork
    {
        protected const string SYS_PREFIX = "SYS";
        protected const string S_PREFIX = "S_";
        protected const string T_PREFIX = "T_";
        protected const string ID_KEY = nameof(EntityBase.ID);

        internal static readonly ILoggerFactory SqlLoggerFactory = LoggerFactory.Create(builder =>
        {
            builder.AddFilter((category, level) => category == DbLoggerCategory.Database.Command.Name && (level == LogLevel.Information));
        });

        public BaseDBContext(DbContextOptions options) : base(options) { }

        /// <summary>
        /// Synchronously, saves all changes made in the UnitOfWork's DbContext to the database.
        /// </summary>
        /// <returns></returns>
        public virtual new int SaveChanges()
        {
            UpdateEntitiesOnAddOrUpdate();
            return base.SaveChanges();
        }

        /// <summary>
        /// Asynchronously, saves all changes made in the UnitOfWork's DbContext to the database.
        /// </summary>
        public virtual new async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            UpdateEntitiesOnAddOrUpdate();
            return await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Transactions allow several database operations to be processed in an atomic manner.
        /// <para>Nested transaction is not allowed in Entity Framework Core. </para>
        /// <para>Usage: unitOfWork.BeginTransaction() </para>
        /// </summary>
        public void BeginTransaction()
        {
            if (this.Database.CurrentTransaction != null)
            {
                throw new MultipleTransactionException("There is a started Transaction detected, please use the other transaction instead of starting a new Transaction.");
            }
            this.Database.BeginTransaction();
        }

        /// <summary>
        /// Synchronously, save all the changes done in the transaction & commit the transaction into the database
        /// <para>Nested transaction is not allowed in Entity Framework Core. </para>
        /// <para>Usage: unitOfWork.CommitTransaction() </para>
        /// </summary>
        public void CommitTransaction()
        {
            if (this.Database.CurrentTransaction == null)
            {
                throw new MultipleTransactionException("Transaction does not exists. Please create one with UnitOfWork.BeginTransaction()");
            }
            SaveChanges();
            this.Database.CommitTransaction();
        }

        /// <summary>
        /// Asynchronously, save all the changes done in the transaction & commit the transaction into the database
        /// <para>Nested transaction is not allowed in Entity Framework Core. </para>
        /// <para>Usage: unitOfWork.CommitTransaction() </para>
        /// </summary>
        public async Task CommitTransactionAsync(CancellationToken cancellationToken = default)
        {
            if (this.Database.CurrentTransaction == null)
            {
                throw new MultipleTransactionException("Transaction does not exists. Please create one with UnitOfWork.BeginTransaction()");
            }
            await SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            this.Database.CommitTransaction();
        }

        /// <summary>
        /// Rollback all of the changes done in the Transaction
        /// <para>Nested transaction is not allowed in Entity Framework Core. </para>
        /// <para>Usage: unitOfWork.RollbackTransaction() </para>
        /// </summary>
        public void RollbackTransaction()
        {
            if (this.Database.CurrentTransaction == null)
            {
                throw new MultipleTransactionException("Transaction does not exists. Please create one with UnitOfWork.BeginTransaction()");
            }
            this.Database.RollbackTransaction();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null)
            {
                optionsBuilder.UseLoggerFactory(SqlLoggerFactory);
                optionsBuilder.UseUpperSnakeCaseNamingConvention();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder != null)
            {
                foreach (var entityType in modelBuilder.Model.GetEntityTypes())
                {
                    var tp = Type.GetType(entityType.ClrType.AssemblyQualifiedName);

                    if (tp != null)
                    {
                        SetTableNameFromTableAttr(tp, entityType, out var tableName);
                        SetKeyNameFromColumnAttr(tp, entityType, tableName);
                    }
                    else
                    {
                        SetDefaultModelCreatingRule(entityType);
                    }
                }
            }

            base.OnModelCreating(modelBuilder);
        }

        private void SetDefaultModelCreatingRule(IMutableEntityType entityType)
        {
            var tableName = entityType.GetTableName();
            var key = entityType.FindProperty(ID_KEY);

            if (key != null)
            {
                var keyName = $"{tableName}_{ID_KEY.ToUpper(CultureInfo.InvariantCulture)}";
                key.SetColumnName(keyName);
            }

            var prefixName = AddPrefixToTableName(tableName);

            entityType.SetTableName(prefixName);
        }

        private string AddPrefixToTableName(string tableName)
        {
            string prefixName;
            if (tableName.StartsWith(SYS_PREFIX, StringComparison.InvariantCultureIgnoreCase))
            {
                prefixName = $"{S_PREFIX}{tableName}";
            }
            else
            {
                prefixName = $"{T_PREFIX}{tableName}";
            }
            return prefixName;
        }

        private void SetTableNameFromTableAttr(Type tp, IMutableEntityType entityType, out string tableName)
        {
            var tbAttr = tp.GetCustomAttribute<TableAttribute>();

            var name = entityType.GetTableName();
            tableName = name;

            if (tbAttr != null && !string.IsNullOrWhiteSpace(tbAttr.Name))
            {
                name = tbAttr.Name;
            }
            else
            {
                name = AddPrefixToTableName(name);
            }

            entityType.SetTableName(name);
        }

        private void SetKeyNameFromColumnAttr(Type tp, IMutableEntityType entityType, string tableName)
        {
            var keyProperty = tp.GetProperty(ID_KEY);
            var columnAttr = keyProperty.GetCustomAttribute<ColumnAttribute>();
            var key = entityType.FindProperty(ID_KEY);
            var keyName = ID_KEY.ToUpper(CultureInfo.InvariantCulture);

            if (columnAttr != null && !string.IsNullOrWhiteSpace(columnAttr.Name))
            {
                keyName = columnAttr.Name;
            }
            else
            {
                keyName = $"{tableName}_{keyName}";
            }

            key.SetColumnName(keyName);
        }

        private void UpdateEntitiesOnAddOrUpdate()
        {
            foreach (var entity in this.ChangeTracker.Entries().Where(e => e.State == EntityState.Modified || e.State == EntityState.Added))
            {
                var saveEntity = entity.Entity as EntityBase;
                if (entity.State == EntityState.Added)
                {
                    saveEntity.Version = 0;
                }
                else if (entity.State == EntityState.Modified)
                {
                    saveEntity.Version += 1;
                }
            }
        }
    }
}

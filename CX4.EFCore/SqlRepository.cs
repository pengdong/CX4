﻿/*
 * Author Dong Peng
 *
 * Created at 2021/9/14 14:24:30 
 *
 */
using CX4.Domain.Repository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CX4.EFCore
{
    public class SqlRepository : ISqlRepository
    {
        private readonly DbContext _dbContext;

        public SqlRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<T> SqlQuery<T>(string sql) where T : class, new()
        {
            return _dbContext.Database.SqlQuery<T>(sql);
        }

        public List<T> SqlQuery<T>(string sql, object parameter) where T : class, new()
        {
            return _dbContext.Database.SqlQuery<T>(sql, parameter);
        }

        public Task<List<T>> SqlQueryAsync<T>(string sql) where T : class, new()
        {
            return _dbContext.Database.SqlQueryAsync<T>(sql);
        }

        public Task<List<T>> SqlQueryAsync<T>(string sql, object parameter) where T : class, new()
        {
            return _dbContext.Database.SqlQueryAsync<T>(sql, parameter);
        }
    }
}

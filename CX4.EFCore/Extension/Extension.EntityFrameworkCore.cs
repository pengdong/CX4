﻿/*
 * Author Dong Peng
 *
 * Created at 2021/9/14 14:42:01 
 *
 */
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Threading.Tasks;

namespace CX4.EFCore
{
    internal static class EntityFrameworkCoreExtension
    {
        private static DbCommand CreateCommand(DatabaseFacade facade, string sql, out DbConnection connection, params object[] parameters)
        {
            var conn = facade.GetDbConnection();
            connection = conn;
            conn.Open();
            var cmd = conn.CreateCommand();
            if (facade.IsRelational())
            {
                cmd.CommandText = sql;
                cmd.Parameters.AddRange(parameters);
            }
            return cmd;
        }

        public static DataTable SqlQuery(this DatabaseFacade facade, string sql, params object[] parameters)
        {
            var command = CreateCommand(facade, sql, out DbConnection conn, parameters);
            var reader = command.ExecuteReader();
            var dt = new DataTable();
            dt.Load(reader);
            reader.Close();
            conn.Close();
            return dt;
        }

        public static async Task<DataTable> SqlQueryAsync(this DatabaseFacade facade, string sql, params object[] parameters)
        {
            var command = CreateCommand(facade, sql, out DbConnection conn, parameters);
            var reader = await command.ExecuteReaderAsync();
            var dt = new DataTable();
            dt.Load(reader);
            reader.Close();
            conn.Close();
            return dt;
        }

        public static List<TEntity> SqlQuery<TEntity>(this DatabaseFacade facade, string sql, params object[] parameters) where TEntity : class, new()
        {
            var dt = SqlQuery(facade, sql, parameters);
            return dt.ToList<TEntity>();
        }

        public static async Task<List<TEntity>> SqlQueryAsync<TEntity>(this DatabaseFacade facade, string sql, params object[] parameters) where TEntity : class, new()
        {
            var dt = await SqlQueryAsync(facade, sql, parameters);
            return dt.ToList<TEntity>();
        }

        public static List<TEntity> ToList<TEntity>(this DataTable dt) where TEntity : class, new()
        {
            var propertyInfos = typeof(TEntity).GetProperties();
            var list = new List<TEntity>();
            foreach (DataRow row in dt.Rows)
            {
                var model = new TEntity();
                foreach (PropertyInfo p in propertyInfos)
                {
                    if (dt.Columns.IndexOf(p.Name) != -1 && row[p.Name] != DBNull.Value)
                    {
                        p.SetValue(model, row[p.Name], null);
                    }
                }
                list.Add(model);
            }
            return list;
        }
    }
}

﻿/*
 * Author Dong Peng
 *
 * Created at 2021/9/13 11:16:15 
 *
 */
using System;

namespace CX4.EFCore
{
    [Serializable]
    internal class MultipleTransactionException : Exception
    {
        public MultipleTransactionException() : base() { }
        public MultipleTransactionException(string message) : base(message) { }
        public MultipleTransactionException(string message, Exception innerException) : base(message, innerException) { }
        protected MultipleTransactionException(System.Runtime.Serialization.SerializationInfo serializationInfo, System.Runtime.Serialization.StreamingContext streamingContext) : base(serializationInfo, streamingContext) { }
    }
}

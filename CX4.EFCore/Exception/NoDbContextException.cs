﻿/*
 * Author Dong Peng
 *
 * Created at 2021/9/13 15:42:41 
 *
 */
using System;

namespace CX4.EFCore
{
    [Serializable]
    internal class NoDbContextException : Exception
    {       
        public NoDbContextException() : base() { }
        public NoDbContextException(string message) : base(message) { }
        public NoDbContextException(string message, Exception innerException) : base(message, innerException) { }
        protected NoDbContextException(System.Runtime.Serialization.SerializationInfo serializationInfo, System.Runtime.Serialization.StreamingContext streamingContext) : base(serializationInfo, streamingContext) { }
    }
}

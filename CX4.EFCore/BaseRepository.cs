﻿/*
 * Author Dong Peng
 *
 * Created at 2021/9/13 15:39:56 
 *
 */
using CX4.Domain.Entity;
using CX4.Domain.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CX4.EFCore
{
    public class BaseRepository<TEntity> : BaseRepository<TEntity, long> where TEntity : class, IEntityBase<long>
    {
        public BaseRepository(DbContext dbContext) : base(dbContext) { }
    }

    public class BaseRepository<TEntity, TKey> : IBaseRepository<TEntity, TKey> where TEntity : class, IEntityBase<TKey>
    {
        protected DbContext DbContext { get; set; }
        protected DbSet<TEntity> DbSet { get; set; }

        public BaseRepository(DbContext dbContext)
        {
            DbContext = dbContext ?? throw new NoDbContextException("EF上下文为空");
            DbSet = DbContext.Set<TEntity>();
        }

        #region 新增
        public TEntity Insert(TEntity entity)
        {
            DbSet.Add(entity);
            return entity;
        }

        public async Task<TEntity> InsertAsync(TEntity entity)
        {
            await DbSet.AddAsync(entity);
            return entity;
        }

        public void InsertRange(IEnumerable<TEntity> entities)
        {
            DbSet.AddRange(entities);
        }

        public async Task InsertRangeAsync(IEnumerable<TEntity> entities)
        {
            await DbSet.AddRangeAsync(entities);
        }
        #endregion

        #region 更新
        public bool Update(TEntity entity)
        {
            return DbSet.Update(entity).State == EntityState.Modified;
        }

        public async Task<bool> UpdateAsync(TEntity entity)
        {
            var flag = DbSet.Update(entity).State == EntityState.Modified;
            return await Task.FromResult(flag);
        }
        #endregion

        #region 删除
        public bool Delete(TKey key)
        {
            var model = Get(key);
            return DbSet.Remove(model).State == EntityState.Deleted;
        }

        public async Task<bool> DeleteAsync(TKey key)
        {
            var model = await GetAsync(key);
            return DbSet.Remove(model).State == EntityState.Deleted;
        }

        public bool Delete(Expression<Func<TEntity, bool>> predicate)
        {
            var list = Query(predicate);
            DbSet.RemoveRange(list);
            return true;
        }

        public async Task<bool> DeleteAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var list = await QueryAsync(predicate);
            DbSet.RemoveRange(list);
            return true;
        }
        #endregion

        #region 查询
        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.FirstOrDefault(predicate);
        }

        public Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.FirstOrDefaultAsync(predicate);
        }

        public TEntity Get(TKey key)
        {
            return DbSet.SingleOrDefault(k => k.ID.Equals(key));
        }

        public async Task<TEntity> GetAsync(TKey key)
        {
            return await DbSet.SingleOrDefaultAsync(k => k.ID.Equals(key));
        }

        public List<TEntity> Query(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Where(predicate).ToList();
        }

        public Task<List<TEntity>> QueryAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Where(predicate).ToListAsync();
        }

        public List<TEntity> Take(Expression<Func<TEntity, bool>> predicate, int count)
        {
            return DbSet.Where(predicate).Take(count).ToList();
        }

        public Task<List<TEntity>> TakeAsync(Expression<Func<TEntity, bool>> predicate, int count)
        {
            return DbSet.Where(predicate).Take(count).ToListAsync();
        }

        public List<TEntity> TakePage(Expression<Func<TEntity, bool>> predicate, int pageIndex, int pageSize, ref int totalCount)
        {
            var query = DbSet.Where(predicate);
            totalCount = query.Count();
            return query.Skip((pageIndex - 1) * pageSize).Take(pageSize).OrderByDescending(k => k.ID).ToList();
        }

        public Task<List<TEntity>> TakePageAsync(Expression<Func<TEntity, bool>> predicate, int pageIndex, int pageSize, ref int totalCount)
        {
            var query = DbSet.Where(predicate);
            totalCount = query.Count();
            return query.Skip((pageIndex - 1) * pageSize).Take(pageSize).OrderByDescending(k => k.ID).ToListAsync();
        }

        public List<TEntity> GetAllList()
        {
            return DbSet.ToList();
        }

        public Task<List<TEntity>> GetAllListAsync()
        {
            return DbSet.ToListAsync();
        }
        #endregion

        #region 统计

        public int Count(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Where(predicate).Count();
        }

        public async Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await DbSet.Where(predicate).CountAsync();
        }

        public decimal Sum(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, decimal>> selector)
        {
            return DbSet.Where(predicate).Sum(selector);
        }

        public async Task<decimal> SumAsync(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, decimal>> selector)
        {
            return await DbSet.Where(predicate).SumAsync(selector);
        }

        public decimal? Sum(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, decimal?>> selector)
        {
            return DbSet.Where(predicate).Sum(selector);
        }

        public async Task<decimal?> SumAsync(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, decimal?>> selector)
        {
            return await DbSet.Where(predicate).SumAsync(selector);
        }

        public int Sum(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, int>> selector)
        {
            return DbSet.Where(predicate).Sum(selector);
        }

        public async Task<int> SumAsync(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, int>> selector)
        {
            return await DbSet.Where(predicate).SumAsync(selector);
        }

        public TResult Min<TResult>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TResult>> selector)
        {
            return DbSet.Where(predicate).Min(selector);
        }

        public async Task<TResult> MinAsync<TResult>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TResult>> selector)
        {
            return await DbSet.Where(predicate).MinAsync(selector);
        }

        public TResult Max<TResult>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TResult>> selector)
        {
            return DbSet.Where(predicate).Max(selector);
        }

        public async Task<TResult> MaxAsync<TResult>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TResult>> selector)
        {
            return await DbSet.Where(predicate).MaxAsync(selector);
        }

        public decimal? Average(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, decimal>> selector)
        {
            return DbSet.Where(predicate).Average(selector);
        }

        public async Task<decimal?> AverageAsync(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, decimal>> selector)
        {
            return await DbSet.Where(predicate).AverageAsync(selector);
        }

        #endregion

        #region 验证
        public bool Exist(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Any(predicate);
        }

        public async Task<bool> ExistAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await DbSet.AnyAsync(predicate);
        }
        #endregion
    }
}

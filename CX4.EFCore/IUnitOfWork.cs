﻿/*
 * Author Dong Peng
 *
 * Created at 2021/9/13 10:52:45 
 *
 */
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CX4.EFCore
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        void BeginTransaction();
        void CommitTransaction();
        Task CommitTransactionAsync(CancellationToken cancellationToken = default);
        void RollbackTransaction();
    }
}

﻿/*
 * Author Dong Peng
 *
 * Created at 2021/9/14 15:36:38 
 *
 */
using CX4.Domain.Repository;
using CX4.EFCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="options"></param>
        public static IServiceCollection AddEFDbContext<TContext>(this IServiceCollection services, Action<DbContextOptionsBuilder> optionsAction = null) where TContext : BaseDBContext
        {
            services.AddDbContext<TContext>(optionsAction);
            services.AddScoped<ISqlRepository, SqlRepository>();
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped(typeof(IBaseRepository<,>), typeof(BaseRepository<,>));
            return services;
        }
    }
}

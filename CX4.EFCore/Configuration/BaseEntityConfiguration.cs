﻿/*
 * Author Dong Peng
 *
 * Created at 2021/9/13 10:45:40 
 *
 */
using CX4.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CX4.EFCore.Configuration
{
    public class BaseEntityConfiguration<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : EntityBase
    {
        /// <summary>
        /// Configures the behavior of the property in BaseEntity
        /// </summary>
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            if (builder != null)
            {
                builder.HasKey(b => b.ID);
                builder.Property(b => b.Version).IsConcurrencyToken(true);
            }
        }
    }
}
